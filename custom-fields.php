<?php
/*
 * Custom fields definition for post type "Story"
 */

$fields = array(
    'name' =>
      array(
        'type'  => 'text',
        'required' => true,
        'rules' => '/[a-z]+/i',
        'message' => 'Name should contain only lowercase and uppercase letters.'
      ),
    'photo' =>
      array(
        'type' => 'file',
        'required' => false,
        'rules' => '',
        'message' => ' - '
      ),
    'url' =>
      array(
        'type' => 'text',
        'required' => false,
        'rules' => '/^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/i',
        'message' => 'URL should be valid (http:, https: or ftp: protocols)'
      ),
    'story_photo' =>
      array(
        'type' => 'file',
        'required' => true,
        'rules' => '',
        'message' => 'Select a file to proceed...'
      )
);

function story_fields_author() {
    global $post, $fields;
    $custom = get_post_custom($post->ID);
    foreach ( $fields as $name => $opts ) {
        $type = $opts['type'];
        $var_name = "author_$name";
        global $$var_name;
        $$var_name = $custom["story_$var_name"][0];
        story_new_field($name, $type);
    }
?>
    <style type="text/css">
        .story_author_error {
            color: #FF0000;
        }
    </style>
    <script type="text/javascript">
        var fields = (<?php echo json_encode($fields); ?>);
        jQuery(document).ready(function($) {
            $("#publish").bind("click", function(e) {
                var $this = $(this);
                var inputValue;
                var rgx, mod, pttn;
                var errors = false;
                // regex validation for predefined fields
                for ( var i in fields ) {
                    rgx = fields[i].rules;
                    if ( rgx.length ) {
                        mod = rgx.substr(-1);
                        rgx = rgx.substr(1, rgx.substr(1).length - 2);
                        pttn = new RegExp(rgx, mod);
                    }
                    inputValue = $('input[name="story_author_' + i + '"]').val();
                    if ( (rgx && ! pttn.test(inputValue))
                        ||
                        (fields[i].required && inputValue == '')
                     ) {
                        $('.story_author_error_' + i).html(fields[i].message);
                        errors = true;
                    }
                }
                // title and content default inputs validation
                $("#title, #content").each(function() {
                    var thisObj;
                    if ( $.trim($(this).val()) == '' ) {
                        ( $(this).attr("id") == "content" ? $("#content_tbl") : $(this) ).css("border", "1px solid #FF0000");
                        error = true;
                    }
                });
                if ( errors ) {
                    e.preventDefault();
                    window.setTimeout(function() {
                        $this.removeClass("button-primary-disabled").parent().find('.spinner').hide();
                    }, 500);
                }
            });
        });
    </script>
<?php
}

function story_new_field($name, $type) {
    $var_name = "author_$name";
    global $$var_name;
?>
    <div>
        <?php if ( $type == 'file' ) : ?>
            <div class="img img-<?php echo $name; ?>">
                <img src="<?php echo $$var_name; ?>" width="100%" />
            </div>
        <?php endif; ?>
        <label><?php echo ucfirst(str_replace('_', ' ', $name)); ?>:</label>
        <input type="<?php echo $type; ?>" name="story_author_<?php echo $name; ?>" value="<?php echo $$var_name; ?>" />
    </div>
    <div class="story_author_error story_author_error_<?php echo $name; ?>"></div>
<?php
}