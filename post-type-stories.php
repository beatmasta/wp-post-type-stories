<?php
/**
 * @package Post Type Stories
 * @version 0.1
 */
/*
Plugin Name: Post Type Stories
Description: Plugin to add new post type "Stories". It adds a new "Stories" section in the wp-admin sidebar.
Author: Alex Vanyan
Version: 0.1
Author URI: http://alex-v.net/
*/

if ( ! function_exists('post_type_stories_register') ) :

function post_type_stories_register() {

    $labels = array(
        'name' => _x('Stories', 'post type general name'),
		'singular_name' => _x('Story', 'post type singular name'),
		'add_new' => _x('Add New', 'story'),
		'add_new_item' => __('Add New Story'),
		'edit_item' => __('Edit Story'),
		'new_item' => __('New Story'),
		'view_item' => __('View Story'),
		'search_items' => __('Search Stories'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
        'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => plugins_url( 'icon.png' , __FILE__ ) ,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title','editor','thumbnail')
	  );

	register_post_type( 'story' , $args );
}
add_action( 'init', 'post_type_stories_register' );

// require field definitions file
require_once dirname(__FILE__) . '/custom-fields.php';

function post_type_stories_fields() {
    add_meta_box("story_author_name-meta", "Author info", "story_fields_author", "story", "side", "high");
}
add_action( 'admin_init', 'post_type_stories_fields' );

function story_add_multipart() {
    echo ' enctype="multipart/form-data"';
}
add_action( 'post_edit_form_tag', 'story_add_multipart' );

function save_post_type_stories_fields($post_id) {
    if ( empty($_POST) || $_POST['post_type'] != 'story' ) return;
    global $post, $fields;
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
        return $post_id;

    $err_stack = array();

    foreach ( $fields as $name => $opts ) {
        $field_name = "story_author_$name";
        if ( $opts['type'] == 'file' && ! ($fields[$name]['file'] = story_upload_file($_FILES[$field_name])) ) {
            $err_stack[$name] = $opts['message'];
        }
        if ( ($opts['required'] && ! $_POST[$field_name] && ! $_FILES[$field_name])
             ||
             ($opts['rules'] && ! preg_match($opts['rules'], $_POST[$field_name]))
        ) {
            $err_stack[$name] = $opts['message'];
        }
    }

    if ( empty($err_stack) ) {
        foreach ( $fields as $name => $opts ) {
            $field_name = "story_author_$name";
            update_post_meta($post->ID, $field_name, ($opts['type'] == 'file' ? $opts['file']['url'] : $_POST[$field_name]));
        }
        return $post_id;
    } else {
        return wp_delete_post($post_id, true);
    }
}
add_action( 'save_post', 'save_post_type_stories_fields' );

function story_upload_file($file) {
    if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );
    $overrides = array( 'test_form' => false );
    return wp_handle_upload( $file, $overrides );
}

endif;